import React from 'react';
import './App.css';
import MessageForm from "./message/form/MessageForm";

function App() {
  return (
    <MessageForm />
  );
}

export default App;
