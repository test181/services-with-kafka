import axios from 'axios';

const BASE_URI = 'http://localhost:9090';

class ApiClient {

    sendMessage(message) {
        return axios.post(BASE_URI + "/message", message);
    }
}

export default new ApiClient();