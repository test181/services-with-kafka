import React from "react";
import {Component} from "react";
import ApiClient from "../client/ApiClient";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";

class MessageForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            message: ""
        };
        this.sendMessage = this.sendMessage.bind(this);
    }

    sendMessage = (e) => {
        e.preventDefault();
        let message = {message: this.state.message};
        ApiClient.sendMessage(message)
            .then(res => {
                this.setState({message: ""});
                alert("Message send successfully")
            });
    };

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <Container maxWidth={"sm"}>
                <h2 className="text-center">Send Message</h2>
                <form>
                    <TextField id="standard-basic"
                               label="Message"
                               name={"message"}
                               value={this.state.message}
                               onChange={this.onChange}
                               fullWidth/>
                    <Button variant="contained" color="primary" onClick={this.sendMessage}>
                        Send
                    </Button>
                </form>
            </Container>
        );
    }
}

export default MessageForm;
