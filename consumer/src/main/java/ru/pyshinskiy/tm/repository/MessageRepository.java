package ru.pyshinskiy.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.entity.Message;

/**
 *
 * @author Pyshinskiy Pavel
 */
@Repository
public interface MessageRepository extends JpaRepository<Message, String> {
}
