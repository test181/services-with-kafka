package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author Pyshinskiy Pavel
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id = UUID.randomUUID().toString();

    private String text;

    public Message(@NotNull final String text) {
        this.text = text;
    }

    public Message(@NotNull final String id, @NotNull final String text) {
        this.id = id;
        this.text = text;
    }
}
