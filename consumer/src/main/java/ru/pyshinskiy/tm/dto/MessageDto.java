package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 *
 * @author Pyshinskiy Pavel
 */
@Getter
@Setter
@NoArgsConstructor
public class MessageDto {

    private String id = UUID.randomUUID().toString();

    private String text;

    public MessageDto(@NotNull final String text) {
        this.text = text;
    }

    public MessageDto(@NotNull final String id, @NotNull final String text) {
        this.id = id;
        this.text = text;
    }
}
