package ru.pyshinskiy.tm.dto;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Message;

/**
 *
 * @author Pyshinskiy Pavel
 */
public class DtoConverter {

    private DtoConverter() {

    }

    @NotNull
    @Contract("_ -> new")
    public static Message toMessageEntity(@NotNull final MessageDto messageDto) {
        return new Message(messageDto.getId(), messageDto.getText());
    }

    @NotNull
    @Contract("_ -> new")
    public static MessageDto toMessageDto(@NotNull final Message message) {
        return new MessageDto(message.getId(), message.getText());
    }
}
