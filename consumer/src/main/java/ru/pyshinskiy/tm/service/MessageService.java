package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.dto.DtoConverter;
import ru.pyshinskiy.tm.dto.MessageDto;
import ru.pyshinskiy.tm.entity.Message;
import ru.pyshinskiy.tm.repository.MessageRepository;

/**
 *
 * @author Pyshinskiy Pavel
 */
@Service
public class MessageService {

    private static final Logger logger = LoggerFactory.getLogger(MessageService.class);

    @NotNull
    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(@NotNull final MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    /**
     *
     * @param messageDto POJO Data-transfer-object
     * @return Persisted entity converted to Dto-object
     */
    @Transactional
    @KafkaListener(topics = "message", groupId = "message")
    public MessageDto save(@NotNull final MessageDto messageDto) {
        logger.info(String.format("$$ -> Consuming message --> %s", messageDto.getText()));
        @NotNull final Message message = DtoConverter.toMessageEntity(messageDto);
        return DtoConverter.toMessageDto(messageRepository.save(message));
    }
}
