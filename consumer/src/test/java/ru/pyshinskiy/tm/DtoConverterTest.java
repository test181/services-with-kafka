package ru.pyshinskiy.tm;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.pyshinskiy.tm.dto.DtoConverter;
import ru.pyshinskiy.tm.dto.MessageDto;
import ru.pyshinskiy.tm.entity.Message;

import java.util.Random;

public class DtoConverterTest {

    @Test
    public void toMessageEntity() {
        @NotNull final MessageDto messageDto = getMessageDto();
        @NotNull final Message message = DtoConverter.toMessageEntity(messageDto);
        Assert.assertEquals(messageDto.getId(), message.getId());
        Assert.assertEquals(messageDto.getText(), message.getText());
    }

    @Test
    public void toMessageDto() {
        @NotNull final Message message = getMessage();
        @NotNull final MessageDto messageDto = DtoConverter.toMessageDto(message);
        Assert.assertEquals(message.getId(), messageDto.getId());
        Assert.assertEquals(message.getText(), messageDto.getText());
    }

    @NotNull
    @Contract(" -> new")
    private Message getMessage() {
        return new Message(new Random().nextInt() + "");
    }

    @NotNull
    @Contract(" -> new")
    private MessageDto getMessageDto() {
        return new MessageDto(new Random().nextInt() + "");
    }
}
