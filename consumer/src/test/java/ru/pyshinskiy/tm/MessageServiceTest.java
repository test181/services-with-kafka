package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.pyshinskiy.tm.dto.MessageDto;
import ru.pyshinskiy.tm.entity.Message;
import ru.pyshinskiy.tm.repository.MessageRepository;
import ru.pyshinskiy.tm.service.MessageService;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {

    @InjectMocks
    private MessageService messageService;

    @Mock
    private MessageRepository messageRepository;

    @Test
    public void messageExistAfterSaving() {
        @NotNull final String randomMessage = new Random().nextInt() + "";
        @NotNull final MessageDto messageDto = new MessageDto(randomMessage);
        when(messageRepository.save(any(Message.class))).thenReturn(new Message(randomMessage));
        assertThat(messageDto.getText()).isSameAs(messageService.save(messageDto).getText());
    }
}
