
# Services-with-kafka

## Project description
Проект демонстрирует работу с сервисом сообщений Kafka посредством двух spring boot приложений и одного web клиента.
Producer предоставляет endpoint для связи с web клиентом и при получении сообщения отправляет его в Kafka.
Consumer слушает соответсвующий topic и при появлении сообщения получает и сохраняет его в БД посредством Spring Data Jpa.


## Installation 
Run
### `mvn clean install`

to build spring boot microservices

### `java -jar producer/target/producer.jar`

to bootstrap producer app and

### `java -jar consumer/target/consumer.jar`

to bootstrap consumer app.

Also you can run:

### `npm start`

to bootstrap react web client.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Technology stack
* Spring Boot 2.2.1.RELEASE
* Apache Maven 3.3+
* Spring-boot-data-jpa
* Spring-boot-web
* Spring-kafka
* Node.js
* React.js
* material-ui

