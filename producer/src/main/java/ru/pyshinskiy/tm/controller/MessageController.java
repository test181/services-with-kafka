package ru.pyshinskiy.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.dto.MessageDto;
import ru.pyshinskiy.tm.producer.Producer;

import java.util.concurrent.ExecutionException;

/**
 *
 * @author Pyshinskiy Pavel
 */
@RestController
@RequestMapping("/message")
public class MessageController {

    @NotNull
    private final Producer producer;

    @Autowired
    public MessageController(@NotNull final Producer producer) {
        this.producer = producer;
    }

    /**
     *
     * @param message POJO Data-transfer-object
     * @return the 'text' field of MessageDto object, saved in Kafka
     * @throws InterruptedException in case of Kafka wrong job
     */
    @PostMapping
    @CrossOrigin(value = "http://localhost:3000")
    public ResponseEntity<String> send(@RequestBody String message) throws InterruptedException {
        try {
            return ResponseEntity.ok(producer.sendMessage(new MessageDto(message)));
        } catch (ExecutionException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Kafka error occurred.");
        }
    }
}
