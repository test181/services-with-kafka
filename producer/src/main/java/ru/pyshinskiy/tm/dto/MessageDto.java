package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 *
 * @author Pyshinskiy Pavel
 */
@Getter
@Setter
public class MessageDto {

    private String id = UUID.randomUUID().toString();

    @NotNull
    private String text;

    public MessageDto(@NotNull final String text) {
        this.text = text;
    }
}
