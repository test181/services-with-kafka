package ru.pyshinskiy.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author Pyshinskiy Pavel
 */
@SpringBootApplication
public class ProducerApp {

    public static void main( String[] args ) {
        SpringApplication.run(ProducerApp.class);
    }
}
