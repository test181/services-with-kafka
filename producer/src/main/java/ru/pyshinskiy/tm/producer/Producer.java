package ru.pyshinskiy.tm.producer;

import org.apache.kafka.clients.admin.NewTopic;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.pyshinskiy.tm.dto.MessageDto;

import java.util.concurrent.ExecutionException;

/**
 *
 * @author Pyshinskiy Pavel
 */
@Service
public class Producer {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);

    @NotNull
    private final NewTopic topic;

    @NotNull
    private final KafkaTemplate<String, MessageDto> kafkaTemplate;

    @Autowired
    public Producer(@NotNull final NewTopic topic,
                    @NotNull final KafkaTemplate<String, MessageDto> kafkaTemplate) {
        this.topic = topic;
        this.kafkaTemplate = kafkaTemplate;
    }

    /**
     *
     * @param message POJO Data-transfer-object
     * @return the 'text' field of MessageDto object, saved in Kafka
     * @throws ExecutionException in case of Kafka wrong job
     * @throws InterruptedException in case of Kafka wrong job
     */
    public String sendMessage(@NotNull final MessageDto message) throws ExecutionException, InterruptedException {
        logger.info(String.format("$$ -> Producing message --> %s", message.getText()));
        return this.kafkaTemplate.send(topic.name(), message)
                .get()
                .getProducerRecord()
                .value()
                .getText();
    }
}
